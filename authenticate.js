module.exports = function (req, res, next) {
  
  
    if (req.session.passport!=undefined && req.session.passport.user) {
        next();
    }
   
    else {
        res.status(401).send({error: 'Not authorized!'});
    }
};