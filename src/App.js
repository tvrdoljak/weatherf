import React from 'react';
import logo from './logo.svg';
import './App.css';
import axios from 'axios';
import WeatherInfo from './WeatherInfo';

class App extends React.Component {

  constructor(props){
     super(props)
     this.state={
       hourlyForecastList:[]
     }
  }
  componentDidMount(){

      axios.get("http://api.openweathermap.org/data/2.5/forecast?id=524901&APPID=ac7ccbe03986e8987745a8cac6eb20b1").then((data)=>{
        this.setState({
          hourlyForecastList:data.data.list
        })
      })

  }
  
  render(){
   return (
   <div className="container">
     Hourly foreacast:
     {this.state.hourlyForecastList.map((hourlyData)=>{
      
       return <WeatherInfo hourlyData={hourlyData}/>
     })}
    </div>
    )
  }
}

export default App;

