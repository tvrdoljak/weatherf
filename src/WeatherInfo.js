import React from 'react';
import logo from './logo.svg';
import './App.css';

class WeatherInfo extends React.Component {

  constructor(props){
     super(props);

     var hdata=props.hourlyData;

     hdata.dateFormatted=new Date(hdata.dt*1000);

     hdata.mainTemperature=parseFloat(hdata.main.temp-273.15).toFixed(2);

    debugger;
     this.state={
        hourlyData:hdata
     }
     

  
  }
  componentWillReceiveProps(props){
      debugger;
      this.setState({
          hourlyData:props.hourlyData
      })
  }
  
  render(){
      const date=this.state.hourlyData.dateFormatted;
   return (
   <div>
      {date.toLocaleDateString()} - {date.getHours() } h - {this.state.hourlyData.mainTemperature} °C
    </div>
    )
  }
}

export default WeatherInfo;

